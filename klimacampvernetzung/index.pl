#!/usr/bin/env perl

use warnings;
use strict;

use CGI;
use Data::Dumper;

sub escape_html {
    my $str = shift;
    $str =~ s/([<>"'&\0])/&#@{[ ord($1) ]};/g;
    return $str;
}

my @output;
my $changes;

my $q = CGI->new();

print $q->header(-type => "text/html; charset=UTF-8");

print <<TMPL;
<!doctype html>
<html lang="de">
<head>
  <title>Klimacampvernetzung</title>
  <meta name="viewport" content="initial-scale=1">
  <style>
    body { font-family: sans-serif; margin-left: auto; margin-right: auto; margin-top: 0; max-width: 55em; }
    .ambassador input { width: 100%; padding: 0.3em; }
    .bare { font-family: inherit; border: 0; background-color: white; text-shadow: 0px 0px 0px; color: black; font-weight: normal; }
    button { font-size: larger; border: 1px solid #e7e7e7; border-radius: 10px; text-shadow: 0px 0px 5px #009ee0; background: linear-gradient(to right, #009ee0 1%,#53b278 100%); color: white; font-weight: bold; }
    th { text-align: left; vertical-align: bottom; }
    tr td:nth-child(3) { text-align: right; }
    tr, p { padding: 0.3em; }
    h1 {
      text-align: center;
    }
    .updated { animation: foo 1s ease-in-out; }
    .highlight {
      background: linear-gradient(to right, #009ee0 1%,#53b278 100%);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    }
    p { text-align: justify; }
    table { border-collapse: collapse; }
    button { padding: 0px; }
    progress { width: 100%; }

    dt { font-weight: bold; }
    dd { margin-bottom: 1em; }
  </style>
</head>
<body>

<h1 class="highlight">Klimacamps</h1>
TMPL

if($q->param("name") or $q->param("phone") or $q->param("city")) {
  open my $fh, ">>", "/home/iblech/klimacampvernetzung.txt" or do {
    print "Sorry -- bitte Ingo unter +49 176 95110311 kontakt dieses Problem informieren";
    exit;
  };

  print $fh Dumper({ $q->Vars });

  print "Danke! Ist notiert :-) Wir melden uns bei dir :-)<br><br>Haut rein in " . escape_html($q->param("city")) . "!</body></html>\n";
  exit;
}

print <<TMPL
<p>Hallo :-) Wir, die Initiator*innen des Augsburger Klimacamps, halten es für eine gute Idee, wenn sich alle deutschen Klimacamps vernetzen würden. Gemeinsam erreichen wir viel mehr als alleine! Wer möchte, kann hier seine Daten eintragen. Sie werden nur auf unserem privaten Server gespeichert und nur zum Zwecke des aktivistischen Austauschs verwendet. Insbesondere werden sie nicht an Dritte weitergegeben. :-)</p>

<form method="post" action="#">
  <dl>
    <dt>Name (evtl. nur Vorname/Pseudonym):</dt>
    <dd><input type="text" name="name"></dd>

    <dt>Stadt:</dt>
    <dd><input type="text" name="city"></dd>

    <dt>Handynummer:</dt>
    <dd><input type="tel" name="phone"></dd>

    <dt>Mail:</dt>
    <dd><input type="mail" name="mail"></dd>

    <dt>Bevorzugte Kontaktart:</dt>
    <dd>
      <input type="radio" name="contact" value="signal" id="signal">
      <label for="signal">Signal</label>

      <input type="radio" name="contact" value="signal" id="telegram">
      <label for="telegram">Telegram</label>

      <input type="radio" name="contact" value="signal" id="whatsapp">
      <label for="whatsapp">WhatsApp</label>

      <input type="radio" name="contact" value="signal" id="mail">
      <label for="mail">Mail</label>
    </dd>

    <dt>Feedback/Anmerkungen:</dt>
    <dd><textarea name="comments" style="width: 100%; height: 10em"></textarea></dd>
  </dl>

  <div style="text-align: center">
    <input type="submit" value="Abschicken">
  </div>
</form>
TMPL
