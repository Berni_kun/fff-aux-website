# 2020-04-07 Protokoll Plenum

* Anwesende AK-Leitungen: Lara, Leon A., Elias, Janika
* Anwesende Delis: Lucia, Luis
* Abwesend: Philipp, Alex, Thomas
* Moderation: Tom
* Protokoll: Tom

[TOC]

## Anliegen der Gäste
* Keine Anliegen von Gästen

## Berichte der AKs und TFs

### AK Struktur (Philipp)
* In Abwesenheit nichts zu berichten.

### AK Demo (Leon A.)
* Keine Neuigkeiten

### AK Mobi (Lara)
* Es gab eine TK in der letzten Woche in der es um die Struktur des AK und um Zukunftplanung ging

### AK Aktionen (Janika)
* Erinnerung an die Foto-Aktion

### AK Kommunikation (Elias)
* Die täglichen Posts sind in der Vorbereitung

### AK For Future (Alex)
* AK-Leitung abwesend und hat keinen Bericht übermittelt.

### TF Strategie (Levin)
* Noch nichts weiter passiert, TF-TK ist ausgefallen

### Deliberichte
* Nichts aus der Bundesebene zu berichten

## Abarbeitung der TOPs

### TOP 1: Deli Finanzabstimmung Web AG
Frist: 07.04.2020

Beschreibung: Die Bedenken wurden im Rahmen einer Kompromiss-Telefonkonferenz ausgeräumt.

Text der Webseiten-AG: 

> Liebe Leute, 🔥🥳
die Web AG will die FridaysForFuture Cloud ausbauen und Website verbessern! ☁🖥
Der Ausbau der Cloud ist natürlich mit einigen Ausgaben verbunden, die aber durch gute Planung und viel ehrenamtliche Arbeit recht niedrig gehalten werden konnte. Um die über 50 Ortsgruppen die schon die Cloud verwenden dies weiter zu ermöglichen und neuen den Einsteig zu ermöglichen, brauchen wir eure Unterstützung ✊🏽
Wir, die Web AG, brauchen eure finanzielle Unterstützung. Daher beantragen wir vom Bundeskonto folgende Mittel:
Der von uns beantragte Finanzrahmen beläuft sich inklusive ausreichender Puffer auf insgesamt 5.500 Euro. Für so eine IT Infrastruktur ist das sehr cheap! Attack zahlt im Jahr fasst 150.000€ nur für Server!
Eine detaillierte Kostenaufstellung findet ihr in der Abstimmung.
Die Abstimmung läuft über 11 Tage bis zum 08.04.
Dieser Ausbau wird krass und wir freuen uns über euren Support! 🌻💚

#### Abstimmungsfrage 1 
Stimmen wir dem Finanzantrag der Webseiten-AG über 5500€ zu? 

**Ergebnis:** 9 dafür, 0 dagegen, 1 Enthaltung

### TOP 2: Rede Ostermarsch

Ergebnis: Levin schreibt eine Rede und schreibt vorher in die Plenumsgruppe, bevor die Rede abgeschickt wird.

### TOP 3: AKs aktivieren
* Regelmäßige TKs etablieren
* Direkt am Anfang jeder TK den nächsten Termin festlegen
* Derzeit ist eine gute Gelegenheit um weiter an der Strategie zu arbeiten
* Es wurden für die kommende Woche folgende Telefonkonferenzen festgelegt:
  * AK Aktionen: Mittwoch, 17:00
  * TF Strategie: Donnerstag, 11:00
  * AK Kommunikation: Samstag, 15:00

### TOP 4: Erinnerung an die Foto-Aktion
* In der Plenums-Telegram-Gruppe findet ihr die Infos dazu.

### TOP 5: Wollen wir auf SocialMedia die Verschiebung des COP26 wegen Corona thematisieren?
Beschreibung: Das Uno-Klimasekretariat und die britische Regierung verschieben den diesjährigen Weltklimagipfel in Glasgow wegen der Corona-Pandemie. Wie erst die finnische Umweltministerin Krista Mikkonen am Mittwochabend via Twitter meldete und kurze Zeit später die britische Regierung bekanntgab, soll die für November angesetzte Konferenz auf 2021 vertagt werden. Der genaue Zeitpunkt steht noch nicht fest.


Die Verschiebung ist ein herber Rückschlag für den Klimaschutz. Der diesjährige Gipfel in Schottland sollte der wichtigste seit langem
werden. Hier sollten die Staaten ihre vor fünf Jahren im Pariser Weltklimaabkommen  vereinbarten Emissionsziele nachbessern.

https://www.spiegel.de/wissenschaft/mensch/corona-krise-uno-klimagipfel-in-glasgow-aus-2021-verschoben-a-ce5ae236-5042-4868-9ea0-915c1c203c26

**Ergebnis:** Der AK-Kommunikation bespricht intern ob und wenn ja wie man das Ausfallen des Klimagipfels thematisieren kann.

### TOP 6: Wollen wir die Gemeinwohllobby auf Social Media bewerben?
* Nein, da wir keine einzelnen Organisationen herauspicken wollen um sie gezielt zu bewerben.

### TOP 7: Instagram Accountname

#### Abstimmungsfrage 7
Sollen wir unser derzeitiges Instagramkonto "fridays.for.future.augsburg" in "fridaysforfuture.augsburg" umbenennen? Es wird nach der Umbenennung ein neues Konto mit dem alten Namen erstellt um eventuelle Links auf unser Konto aufrecht zu erhalten. Das neue Konto mit dem alten Namen wird einen Verweis auf das alte Konto mit dem neuen Namen enthalten.

**Ergebnis:** 10 dafür, 0 dagegen, 1 Enthaltung

### TOP 8: Globaler Streik
Liegt im Aufgabenbereich des AK Aktion und wird während der nächsten AK-Aktion-Telefonkonferenz besprochen.

## Sonstiges

### Messenger-AG
* Mögliche Gruppen in denen man die Dienste der Messenger-AG (auch bekannt als Chatnetiquette-AG) einsetzen könnte
  * Diskussionsgruppe auf WhatsApp
  * Austausch-Gruppe auf WhatsApp
#### Abstimmungsfrage S1 Version 2
Die bundesweite Messenger-AG entsendet mehrere Admins in die Augsburger Gruppen "Diskussion", "Austausch" um Spam und Trollversuche zu bekämpfen und um zu moderieren, falls es zu Streitigkeiten kommt. Es wird das bundesweite Regelwerk eingesetzt.

**Ergebnis:** 8 dafür, 0 dagegen, 3 Enthaltungen

### BigBlueButton
* Werbeblock BigBlueButton

Das Protokoll wurde verlesen: Nein, da alle kollaborativ an diesem CryptPad mitarbeiten konnten.

Das Plenum endete um 21:00 Uhr