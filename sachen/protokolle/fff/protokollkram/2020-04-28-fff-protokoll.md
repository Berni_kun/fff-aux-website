# 2020-04-28 Protokoll Plenum

* Anwesende AK-Leitungen: Lara, Leon, Elias, Janika
* Anwesende Delis: Lucia, Luis
* Abwesend: Thomas, Philipp, Alex
* Moderation: Tom
* Protokoll: Tom

[TOC]

## Anliegen der Gäste
* Keine Anliegen

## Berichte der AKs und TFs

### AK Struktur (Philipp)
* Es wird eine TK stattfinden, für die noch ein Croodle erstellt wird.

### AK Demo (Leon A.)
* Nichts zu berichten

### AK Mobi (Lara)
* Morgen, Mittwoch, den 29.04.2020 findet um 18:00 Uhr eine TK statt

### AK Aktionen (Janika)
* Rathausplatz-Plakat-Aktion fand statt, die Demoschilder befinden sich auf dem Rückweg zu ihren Besitzer:innen
* Die Aktion war sehr erfolgreich und brachte uns auch mediale Aufmerksamkeit

### AK Kommunikation (Elias)
* Die Tagesschau berichtete über deutschlandweite Aktionen am 24.04.2020, auch andere überregionale Medien wie die Süddeutsche Zeitung berichteten
* Die Collagen und Demobilder haben eine deutlich größere Reichweite erreicht als unsere sonstigen Beiträge

### AK For Future (Alex)
* Nichts zu berichten

### TF Strategie (Levin)
* Es wird eine weitere TK stattfinden, voraussichtlich dieses Wochenende

### Deliberichte
* Nichts zu berichten, was nicht schon durch eigenständige TOPs abgedeckt wird

### Kafka-Berichte
* Strukturänderungen an der Bundesstruktur: Es gibt zwei Anträge, über die noch gesprochen und abgestimmt werden muss

## Abarbeitung der TOPs

### TOP 1: Deli Finanzabstimmung "WirBildenZukunft"
* #WirBildenZukunft* beantragt 2500€ für Aufwandsentschädigungen für selbständige Referent*innen.

#### Abstimmungsfrage 1
Unsere OG genehmigt den Finanzantrag von #WirBildenZukunft über 2500€.

**Ergebnis**: 3 Dafür, 9 Dagegen, 0 Enthaltungen

**Begründung:** Wir lehnen es ab, weil wir gegen ein System sind, in dem wir manche Referent:innen bezahlen und andere nicht. Wir finden, dass wir entweder alle bezahlen müssen oder alle Referent:innen ihre Webinare ehrenamtlich durchführen möchten.


### TOP 2: Deli Finanzabstimmung OG Osnabrück

Beschreibung: Die *OG Osnabrück* beantragt  873,91 € für Mobi-Materialien.

#### Abstimmungsfrage 2
Die OG genehmigt den Finanzantrag der OG Osnabrück.

**Ergebnis**: 9 Dafür, 1 Dagegen, 2 Enthaltungen

### TOP 4: Koalitionsprogramm lesen

Die CSU und die Grünen veröffentlichten ihr Koalitionsprogramm. Wir werden uns damit auseinandersetzen um möglichst viel Klimagerechtigkeit aus der neuen Stadtregierung herauszuholen.

* [Link 1](https://www.daz-augsburg.de/schwarz-gruen-folgt-einem-waehlerauftrag-kommentar-zur-bildung-der-neuen-augsburger-stadtregierung/)
* [Link 2](https://www.daz-augsburg.de/gruener-gehts-nicht-kommentar-zum-koalitionsvertrag-der-schwarz-gruenen-stadtregierung/)

## Sonstiges
### Zukunftspreis
Wir können uns noch bis Donnerstag für den Zukunftspreis bewerben. Wir werden eine verbesserte Version der Bewerbung des letzten Jahres erstellen und einreichen.

Das Protokoll wurde verlesen: Nein, aber alle konnten am kollaborativen CryptPad in Echtzeit mitlesen.

Das Plenum endete um 20:30 Uhr