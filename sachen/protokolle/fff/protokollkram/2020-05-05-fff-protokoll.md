# 2020-05-05 Protokoll Plenum

* Anwesende AK-Leitungen: Lara, Janika, Elias, Leon 
* Anwesende Delis: Luis, Lucia
* Abwesend: Thomas
* Moderation: Tom
* Protokoll: Tom

[TOC]

## Anliegen der Gäste
* Es war während des Plenums ein Gast anwesend

## Berichte der AKs und TFs

### AK Struktur (Philipp)
* Derzeit nichts zu berichten

### AK Demo (Leon A.)
* Derzeit nichts zu berichten

### AK Mobi (Lara)
* Es gab eine TK am Mittwoch bei der unter anderem über Sticker gesprochen wurde
* Es soll Kontakt mit den Teachers for Future aufgebaut werden, es soll ein Delegiertensystem wiederaufgebaut werden, sodass wir pro Schule je eine:n Vertreter:in haben

### AK Aktionen (Janika)
* Derzeit nichts zu berichten

### AK Kommunikation (Elias)
* Das Aftermovie des 1.03. ist fast fertig und kann demnächst an einem passenden Tag hochgeladen werden
* Es fand ein Interview mit einer Studentin statt, die die Kommunikationsstrukturen von FFF und XR Augsburg erforschen möchte
* Unsere E-Mail-Adressen enden vorerst auf .is, es ist derzeit unklar wie lange

### AK For Future (Alex vertreten durch Ingo)
* Vernetzungstreffen deutschlandweit aller "müssen handeln"-Bündnisse diesen Donnerstag (u.A. Hamburg)
* Radentscheid läuft wieder an

### TF Strategie (Levin)
* Derzeit nichts zu berichten

### Deliberichte

### Kafka-Berichte
* [Kafka Strukturänderungen an der Bundesstruktur zum Strukturänderungsantrag der FINTA* TF](https://cryptpad.rebellieren.jetzt/code/#/2/code/view/KCMfEnre9RxjJBn2WCAPmGKFXY7QSMYDC2wQDXaAaQM/embed/present/)
* [Kafka Strukturänderungen an der Bundesstruktur zum Handlungsspielraum von AGs](https://pad.fridaysforfuture.is/p/r.d5fe40f5bf2cfbcd223aa8c939a334a7)

## Abarbeitung der TOPs

### TOP 1: Baum Demo: XR lädt uns ein

Tom // 03.05.2020, 22:07 Uhr

Frist: 14.05.2020

Beschreibung:
Baum-"Demo" vom 15. auf den 16. Mai!

🌳Was? Alle die mitmachen wollen basteln Schilder 
🌳Wann? Schilder werden dann am vom 15 auf den 16 Mai aufgehängt (falls es regnen sollte wird Aktion verschoben)
🌳Wo? Plakate z. B. mit einer Schnur am Baum befestigen: im Siebentischwald (nähe Hochablass, Stempflesee, Kuhsee), falls es nicht möglich ist die Schilder dort aufzuhängen gerne auch an einer anderen stelle

Schilder sollen für Bäume sprechen (ungefähr so: https://youtu.be/xwdn0xgoLVk)

🌳Plakat: Spruch auf einen Karton schreiben und je rechts und links 4 Stöcke befestigen, sodass es aussieht als hätte der Baum Finger und würde das Plakat halten (Beispiel im Bild)

🌳Karton kann oft im Supermarkt mitgenommen werden… einfach nachfragen :)

🌳Ideen für Sprüche findet ihr im Pad oder im Video

🌳 Das Pad: https://cryptpad.fr/pad/#/2/pad/edit/wjSigSGBIdmLKsxxwbN9EMix/

#### Abstimmungsfrage 1 (ehemals 2):
Der AKK darf Bilder der Baum-Demo-Aktion auf sozialen Medien teilen. Alle Bilder enthalten nur Demoschilder, von deren Maler:innen wir die Zustimmung für die Verwendung für die Baum-Aktion bekommen haben. Es werden keine Demo-Schilder vom 24.04. ohne die Zustimmung der Maler:innen aufgehängt.

**Ergebnis:** 11 dafür, 2 dagegen, 0 Enthaltungen

### TOP 2: Antrag der Struktur-AG auf Änderung ihres Handlungsspielraums (Deli-Abstimmung)

Frist: 14.05.2020, 21 Uhr

- Die Struktur-AG hat eine Änderung ihres Handlungsspielraums nach 5.8 des Strukturpapiers beantragt.

- Über den Antrag wird regulär abgestimmt. Zur Annahme ist eine einfache Mehrheit erforderlich.

- Hier der Änderungsantrag: https://fffutu.re/aenderungstrukturag

#### Abstimmungsfrage 2 (ehemals 3)
Unsere OG lehnt den Antrag der bundesweiten Struktur-AG auf Änderung ihres Handlungsspielraums ab. Die Kafka für Änderungen der Bundesstruktur erstellt eine Begründung und kann im Anschluss selbst bestimmen, ob aus der Ablehnung sogar ein Veto gemacht werden muss.

**Ergebnis:** 11 Dafür, 0 Dagegen, 2 Enthaltungen

### TOP 3: Beim BUND engagieren?

Der BUND Naturschutz hat in Augsburg zu wenig Menschen, die sich engagieren. Der BUND hat viel Wirkmacht auf politischer Ebene. Jemand aus dem BUND Augsburg hat uns gefragt, ob wir uns nicht beim BUND engagieren möchten um unsere Wirkung zu vergrößern.

Ergebnis: Kooperation mit dem BUND sinnvoll, aber wir sind bei FFF Augsburg nicht "zu viele", sodass wir es unseren Aktivist:innen nicht nahelegen würden sich aus FFF zu verabschieden und nur noch woanders aktiv zu sein.

### TOP 4: Tipps an Teachers for Future Augsburg

Wir stellen Verbindung mit den Teachers for Future her, die sich gerade gegründet haben.

### TOP 5: Austausch mit dem kleinen Agendateam

Wir treffen uns mit zwei Sprecher*innen der lokalen Agenda 21 sowie zwei Menschen aus dem Büro für Nachhaltigkeit. Das Treffen findet am 18.5. um 17:00 Uhr digital statt. Janika und Leon Ü. nehmen am Treffen teil.

## Sonstiges
### Werbeblock
  * Janika: Das vegane Café Dreizehn bringt im Juni ein Kochbuch raus, wer mag kann es vorbestellen.

Das Protokoll wurde verlesen: Nein, aber alle konnten am kollaborativen CryptPad in Echtzeit mitlesen.

Das Plenum endete um 22:20 Uhr