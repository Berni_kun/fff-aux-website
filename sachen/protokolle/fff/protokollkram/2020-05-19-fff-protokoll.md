# 2020-05-19 Protokoll Plenum

* Anwesende AK-Leitungen: Leon A., Janika, Lara und natürlich Elias
* Anwesende Delis: Lucia, Luis
* Abwesend: Philipp, Alex, Thomas

[TOC]

## Anliegen der Gäste

* Es gibt keine Gäste.

## Berichte der AKs und TFs

### AK Struktur (Philipp)

* Nichts Neues.

### AK Demo (Leon A.)

* Nichts Neues.

### AK Mobi (Lara)
* In der letzten TK wurde hauptsächlich über Flyer geredet, Verteilung, Auslagen, Herstellung

### AK Aktionen (Janika)
* Nichts Neues.

### AK Kommunikation (Elias)

### AK For Future (Alex vertreten durch Ingo)

* Bessere Kontrolle der Stadtratssitzungen -- es wird eine Telegramgruppe geben, in der die Tagesordnung übersichtlich gepostet werden wird, andere Vorschläge der Umsetzung sind willkommen
* Information: Möglichkeit eines Klimaentscheids
  * Die rechtliche Prüfung und Vorbereitung wurde bereits durch GermanZero_ übernommen, d.h. juristische Kosten gäbe es keine
* vom Klimarat organisiert: Critical Mass am morgigen Mittwoch
* vom Klimarat organisiert: Gehzeugdemo am Samstag

### TF Strategie (Levin)

* TK fiel aus
* Umfrage für Ziele ist fertig
* Das Pad zur Dokumentation der Nichtteilnahme an Strategie-TKs freut sich noch über Beiträge

### Deliberichte

* Deli-TK fiel aus (!) wegen Problemen mit Zoom...
* In Zukunft wird hoffentlich BigBlueButton verwendet

### Kafka-Berichte

* Kafka zum Selbstverständnis: Sammlung in Pad läuft
* Kafka zu Strukturänderungen an der Bundesstruktur: nichts Neues

## Abarbeitung der TOPs

### TOP 1: Termin Plenum

Es wird besprochen, ob man das Plenum wieder auf eine frühere Uhrzeit verlegen sollte und ob eine Pause eingerichtet werden soll. Die Mehrheit scheint laut Stimmungsbild 19:00 Uhr zu präferieren.

#### Abstimmungsfrage 4
Sollen wir während unserem Online-Plenum spätestens um 20:30 Uhr eine feste Pause von 15 Minuten machen?

**Ergebnis**: 10 dafür, 1 dagegen, 1 Enthaltung


### TOP 2: Neue Telegram Gruppe (Plenum Diskussion)

Wir verschieben derzeit viele Diskussionen in den Telegram-Plenumsgruppen. Eventuell ist es in Zukunft sinnvoller, das in einer extra Gruppe zu machen. 

#### Ergebnis
Es gibt drei Gruppen für die interne Kommunikation der Plenumsmitglieder.

Alle Gruppen/Kanäle
* Meta: Die Gruppen bzw. der Kanal sind nicht öffentlich und nur für aktive Plenumsmenschen zugänglich.

FFF Plenum
* Lesemodus: Verpflichtend. Das heißt, man sollte alle Nachrichten in dieser Gruppe lesen.
* Aufrufe an alle, teilzuhaben
* Fragen zu Handlungsspielräumen
* Eilabstimmungen (inkl. den dazugehörigen Diskussionen)
* Links zu Plenumsprotokollen
* Inhaltlich relevante Links zum Plenum
* Themen, die direkte Relevanz zu FFF Augsburg oder besonders wichtige bundesweite FFF-Themen

Plenum Info-Kanal
* Lesemodus: Optional. Man muss nicht alle Nachrichten lesen.
  * Das heißt, dass wichtige Informationen an alle unbedingt auch in der "Gruppe FFF" Plenum stehen müssen!
* Meta: Alle sind Admins des Kanals. Es ist aktiviert, dass man den:die Absender:in eines Beitrags sehen kann
* Alle Themen rund um Klimagerechtigkeit
* Deliprotokolle
* Aktionsaufrufe von anderen Bewegungen (sofern relevant)

Plenum Info Diskussion
* Lesemodus: Optional. Man muss nicht alle Nachrichten lesen.
* Meta: Diese Gruppe ist mit dem Plenum-Info-Kanal verknüpft. Es erscheinen automatisch alle Nachrichten aus dem Plenum-Info-Kanal in dieser Gruppe.
* Es wird hier auf Beiträge und Links aus der "Plenum Info"-Gruppe reagiert und diese diskutiert
* Es können Diskussionen aus der "FFF Plenum"-Gruppe in diese Diskussionsgruppe verlegt werden
* Themen, die direkte Relevanz zu FFF Augsburg oder besonders wichtige bundesweite FFF-Themen werden in der "FFF-Plenum"-Gruppe besprochen!


### TOP 3: Info über Gespräch mit Agenda

* Es fand ein Treffen zwischen uns (vertreten durch Lara und Leon) und der lokalen Agenda 21 statt.
* Die lokale Agenda 21 wollte wissen, wie wir uns am besten gegenseitig helden können.
* Ab jetzt ist Luis die Kontaktperson zur lokalen Agenda 21 und bekommt die Kontaktdaten unserer Kontaktperson Maria

### TOP 4: Ziele erarbeiten

* Es gibt ein CryptPad-Dokument in dem wir die Ziele für die nächsten drei Monate gesammelt wurden
* Die Ergebnisse werden nun konsolidiert und priotitätsbasiert abgestimmt
* Dabei wird mit Ziffern zwischen 1 bis 5 abgestimmt, wobei 1 unwichtig und 5 sehr wichtig repräsentiert.

## Sonstiges
* Parents for Future digital adoptieren
* Nächstes Plenum schauen die Teachers for Future vorbei

Das Protokoll wurde verlesen: Nein, aber alle konnten am kollaborativen CryptPad in Echtzeit mitlesen.