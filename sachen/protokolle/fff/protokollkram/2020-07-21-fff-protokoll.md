# 2020-07-21 Protokoll Plenum

* Anwesende AK-Leitungen: Levin, Alex, (Leon), (Janika)
* Anwesende Delis: Sophia, (Janika)
* Abwesend: Lucia, (Lara), (Elias)
* Moderation: Ingo, dann Levin
* Protokoll: Ingo, dann Alex

[TOC]

## Anliegen der Gäste

## Berichte der AKs und TFs

### AK Struktur (Levin)
* Mitstreiter*innen gesucht, insbesondere für Online-Offline-Zusammenarbeit.  
### AK Demo (Leon), Mobi (Lara), Aktionen (Janika), Kommunikation (Elias), For Future (Alex)
Heute nichts.

### Deliberichte (Lucia, Janika, Sophia)
Heute nichts.

### Kafka-Berichte
* Awareness (Levin): Ausführlicher Bericht von Awareness-Wünschen, die Levin einsammelte und anonym weitergab.   



## Abarbeitung der TOPs


### Sondertop 1: Awareness
Es gibt gute und ausgereifte Pläne für Onboarding. Es gab dazu bereits zwei produktive Telefonkonferenzen. Levin und Josefine beschäftigen sich schon viel mit dem Thema. Vorschlag: Wir entkoppeln die Technik-Einführung vom eigentlichen Onboarding. Außerdem vertrauen wir Levin und Josefine in der Detailausgestaltung. Hilfe von anderen können sie jederzeit in Anspruch nehmen.

Wir arbeiteten die Geschehnisse der letzten zwei Wochen ein Stück weit auf. Weiterer Klärungsbedarf ist nötig. Dazu soll eine Telefonkonferenz stattfinden. Ingo wird die organisieren.



### TOP 1: Kommunikation des Klimacamps und der Klimarat
*Tom (auch ohne mich besprechen und abstimmen) // 20.07.2020, 19:57 Uhr*
**Frist:** 21.07.2020
**Beschreibung:**
>Es ist dringend notwendig, dass wir jeglichen namentlichen Missbrauch unserer FFF-OG unterbinden. Das Camp soll ein Gemeinschaftsprojekt sein, daher gibt es auch tägliche Camp-Plena bei denen alle einfach so mitentscheiden können. Das macht es aber auch erforderlich, dass die dort entschiedenen Dinge stets als "vom Klimarat" kommuniziert werden und nicht als "von FFF Augsburg". Hier gab es in letzter Zeit viele kommunikative Probleme. Das muss sich schnell ändern, damit wir die Kontrolle über unsere "Marke" FFF-Augsburg behalten und nicht an das Campplenum abgeben.

#### Wunsch:
>Es soll wieder verstärkt darauf geachtet werden, dass das Klimacamp als "vom Augsburger Klimarat" kommuniziert wird. FFF muss in den Hintergrund treten um die kommunikativen Probleme der vergangenen Wochen auszugleichen, die dazu führten, dass das Klimacamp-Plenum effektiv über FFF-Augsburg-Inhalte entscheidet, weil es die Öffentlichkeit und sogar manche Aktivist:innen anderer Bewegungen so wahrnehmen. In allen Veröffentlichungen (auch auf sozialen Medien), Interviews und gegenüber anderen OGs muss angemessen betont werden, dass es sich um ein Gemeinschaftsprojekt handelt und FFF keine führende Rolle mehr übernimmt. Es wird ganz aktiv an der Richtigstellung dieser kommunikativen Ebene gearbeitet, indem unter anderem Posts über Zuständigkeiten und die Diversität der Klimagruppen, die das Klimacamp organisieren, veröffentlicht werden.

* In letzter Zeit haben wir ein bisschen vercheckt, in der Presse klarzustellen, dass das Klimacamp zwar ganz am Anfang von FFF Augsburg gestartet wurde, aber schon sehr schnell in den ersten Tagen von vielen anderen Bewegungen Augsburgs mitgetragen wurde (auch bekannt als Klimarat)
* Wir versäumten bei Presseinterviews klarzustellen, dass nicht FFF Augsburg alleine dieses krasse Klimacamp trägt. Das ist der Verdienst auch vieler anderer Bewegungen.
* es stimmt natürlich, dass FFF Augsburg das Camp gestartet hat, aber alleine hätten wir das nie so weit geschafft 

#### Abstimmungsfrage 1.1:
>Wir bei FFF Augsburg verpflichten uns dazu, das Klimacamp fortan bei jeder Gelegenheit als gemeinsame Aktion vom Klimarat (eine Ansammlung vieler Klimagerechtigkeitsgruppen in Augsburg) und explizit nicht als alleinige Aktion von FFF Augsburg darzustellen, da das der Realität entspricht.

**Ergebnis**: 5 dafür, 0 dagegen, 0 Enthaltungen

#### Abstimmungsfrage 1.2:
>Wir wünschen uns, dass bei passenden Gelegenheiten auf Social Media erwähnt wird, dass das Klimacamp von vielfältigen Klimagerechtigkeitsgruppen Augsburgs durchgeführt und getragen wird. Unseren Social Media Leuten ist es überlassen, ob das einen extra Beitrag kriegt, oder einfach regelmäßig in Beschreibungen klargestellt wird.
Außerdem soll auf jeglichem medialen Output nicht die FFF Designidentität oder das FFF-Logo verwendet werden, um Klimacampaktionen zu bewerben. Außer wenn es vom FFF Plenum beschlossene FFF Aktionen sind, die das Klimacamp als Ort einbinden.

**Ergebnis**: 5 dafür, 0 dagegen, 0 Enthaltungen



### TOP 2: Insta-Account der Studis
*Ingo // 29.06.2020, 14:18 Uhr*
Es gibt keine Frist.
**Beschreibung:**
>Die bisherigen Betreuerinnen des Insta-Account der Studis (mehr als 400 Follower) haben keine Lust mehr, den Account zu betreuen, da sie trotz immer wieder wiederholter Bitte nie in den Posts des Haupt-FFF-Accounts markiert wurden und so immer extra Arbeit hatten. Zu Beginn machte ihnen der Account voll Spaß, aber mittlerweile fühlen sie sich nicht mehr wertgeschätzt.
Wir benötigen daher neue Betreuer*innen für den Insta-Account der Studis. Wer hat Lust?

* wir schlagen vor, dass für den Studis FFF Augsburg Instagram Account erstmal keine Ersatzleitung gesucht wird, da es aktuell keine Präsenzveranstaltungen gibt und wir keine starke Studis-Community haben, die auch in nicht-präsenz-Zeiten Sachen organisieren. Bzw alle unsere aktiven Studis helfen direkt bei FFF Augsburg mit, etwa beim Klimacamp.
* es wird trotzdem schon ab jetzt der Studis Account immer verlinkt
* wir verpflichten uns alle dazu, aufzupassen, dass unsere Social Media Leute vom FFF Augsburg Account auch den Studis Account markieren



## Sonstiges

Das Protokoll wurde verlesen: Nein, aber alle konnten am kollaborativen CryptPad in Echtzeit mitlesen.

Das Plenum endete um 21:10 Uhr