# 2020-04-21 Protokoll Plenum

* Anwesende AK-Leitungen: Philipp, Leon A., Lara, Janika, Elias, Levin (TF)
* Anwesende Delis: Lucia, Luis
* Abwesend: Alex, Thomas
* Moderation: Tom
* Protokoll: Tom und Ingo

[TOC]

## Anliegen der Gäste
* Es gibt keine Anliegen von Gästen.

## Berichte der AKs und TFs

### AK Struktur (Philipp)
* Es gibt ein Doodle das sich noch auf Teilnehmer:innen freut

### AK Demo (Leon A.)
* Es ist nichts passiert.

### AK Mobi (Lara)
* Es soll Sticker geben, die sich nicht auf ein bestimmtes Datum beziehen
* in einer TK wurde gesprochen, wie wir junge Menschen motivieren können

### AK Aktionen (Janika)
* Schilder, Schilder, Schilder! Alle sollen Schilder für den 24.04. malen.
* Wir waren auf Bayern3 in den Nachrichten!

### AK Kommunikation (Elias)
* Bitte reicht noch mehr Ideen für die Mottotage auf den sozialen Medien ein! Beispielsweise gab es heute keinen Divestment-Dienstag-Post.

### AK For Future (Alex)
* Es gibt derzeit keine Neuigkeiten.

### TF Strategie (Levin)
* Es fand eine produktive TK statt, in der aber noch Platz für noch mehr Beteiligung gewesen wäre

### Deliberichte
#### Bundes-Delis
* Die Bamberger-Strukturänderungen wurden zurück in die Feedbackphase gestimmt und es wird eine neue, verbesserte Version zur Abstimmung gestellt

### Kafka-Berichte
* [Link zum Protokoll](https://cryptpad.rebellieren.jetzt/code/#/2/code/view/9A1VJPgI3eRqGzDAT16Hnq3imZ9-R1UzSBo3iY1pTIk/embed/present/)

## Abarbeitung der TOPs

### TOP 1: Schilder

Es wird eine Aktion am 24.04.2020 stattfinden, für die viele Demo-Schilder gebraucht werden. Hiermit sind noch einmal alle dazu aufgerufen, Schilder zu malen und kontaktlos bei der [Sammelstelle in ihrem Stadtteil](https://pad.fridaysforfuture.de/p/r.71b6b1ac0e75676467ee8021835a92e5) abzugeben.

### TOP 2: 24.04.
* Es werden viele Demo-Schilder gemalt, die an Sammelstellen gesammelt werden
* Damit die Schilder nicht wegfliegen brauchen wir noch viele Steine um die Schilder zu beschweren
* Die kontaktlose Schilder-Demo beginnt um 11:00 Uhr am Königsplatz und wechselt um 14:00 Uhr zum Rathausplatz, dort geht die Demo bis 17:00 Uhr bevor abgebaut wird und die Schilder den Nachhauseweg antreten


### TOP 3: Sticker

Wir werden jetzt Sticker erstellen, die zeitlos sind und Menschen dazu motivieren sollen sich uns anzuschließen. In der Vergangenheit waren die meisten Sticker auf einen bestimmten Streiktermin abgestimmt.

### TOP 4: Vernetzungskongress Bayern & Baden-Württemberg?

Beschreibung der Bundes-Abstimmung: Wir würden gerne einen zwei- bis viertägigen Vernetzungskongress für die Länder Baden-Württemberg und Bayern, eine Ba(c)Kon organisieren. Vom Grundprinzip soll er dem Nordkongress ähneln. 
Als Standort dachten wir an einen Ort, der zentral und gut mit der Bahn zu erreichen ist. Vom Zeitraum her wird es in Ferien sein, die es sowohl in Bayern als auch in Baden-Württemberg gibt. Wir werden natürlich auch mit Corona abwarten müssen, wie sich die Lage entwickelt. Es soll auf jeden Fall keine Konkurrenz mit dem SoKo entstehen.

#### Abstimmungsfrage 4
Meine Ortsgruppe möchte eine Süd-Konferenz (SüKo).

**Ergebnis:**: Dafür 13, Dagegen 1, Enthaltung 0

### TOP 5: Finanzantrag OG Oldenburg

Die OG Oldenburg beantragt für bereits entstandene Kosten für den 24.04. 1263,48 EUR.


#### Abstimmungsfrage 5
Wir als Ortsgruppe genehmigen die beantragten 1263,48 € für die OG Oldenburg

**Ergebnis:**: Dafür 11, Dagegen 0, Enthaltung 3

### TOP 6: FFF-Protest zu weltweiter UN-Klimakonferenz in Bonn

Die OG Bonn möchte anlässlich der UN-Klimakonferenz im Oktober mit einer großen Aktion auf das fehlende Handeln aufmerksam machen. Hierzu hat sie uns einige Informationen gegeben, die aber zunächst intern bleiben.

### TOP 7: Inaktive Admins

#### Abstimmungsfrage 7
Auf diversen WhatsApp-Gruppen sind Admins, die derzeit nicht mehr bei uns aktiv sind. Wir überlassen es Levin, der sich im Namen des AKK darum kümmert, inaktiven Admins ihre Adminrechte zu entziehen und aktive Menschen als Admins hinzuzufügen.

**Ergebnis:** 8 Dafür, 0 Dagegen, 3 Enthaltungen


## Sonstiges
* Es gibt nichts sonstiges.

Das Protokoll wurde verlesen: Nein, aber alle konnten am kollaborativen CryptPad in Echtzeit mitlesen.

Das Plenum endete um 21:15 Uhr