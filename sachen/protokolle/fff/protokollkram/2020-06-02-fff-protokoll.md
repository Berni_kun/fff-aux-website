# 2020-06-02 Protokoll Plenum

* Anwesende AK-Leitungen: Alex M., Leon A., Janika, Levin (TF) 
* Anwesende Delis: Lucia
* Abwesend: Philipp, Lara, Elias, Luis
* Turbo-Moderation: Tom
* Premium-Protokoll: Ingo und Tom

[TOC]

## Anliegen der Gäste
* Keine Gäste anwesend

## Berichte der AKs und TFs

### AK Struktur (Philipp)
* Nicht anwesend

### AK Demo (Leon A.)
* Wir hatten heute eine sehr geile Plakatseildemo.
* Vielen lieben Dank an alle, die die heutige Demo ermöglichten: die, die die Idee hatten; die, die die ganze Orga im Blick hatten; die, die Plakate, Wäscheleinen und Wäscheklammern mitbrachten; die, die heute fotografierten; die, die heute Interviews gaben; die, die heute beim Aufbau halfen; die, die heute auf der Demo für Stimmung sorgten; und insgesamt alle, die dabei waren oder an uns dachten ??
* In 67 Städten gab es Aktionen!

### AK Mobi (Lara)
* Nicht anwesend

### AK Aktionen (Janika)
* Nichts spezielles zu berichten

### AK Kommunikation (Elias)
* Nicht anwesend

### AK For Future (Alex)
* Nichts neues zu berichten von Seiten der Students oder Parents
* Persönliche Treffen derzeit noch nicht möglich

### TF Strategie (Levin)
* Nicht anwesend

### Deliberichte
* Es gibt nichts, was separat berichtet werden müsste

### Kafka-Berichte
* Nicht anwesend

## Abarbeitung der TOPs
paar interne Sachen

## Sonstiges

Das Protokoll wurde verlesen: Nein, aber alle konnten am kollaborativen CryptPad in Echtzeit mitlesen.

Das Plenum endete um 23:59 Uhr?