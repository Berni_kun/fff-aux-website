 # 2020-04-14 Protokoll Plenum

* Anwesende AK-Leitungen: Elias, Lara, (Levin), Alex, Janika
* Anwesende Delis: Lucia, Luis
* Abwesend: Phillip, Leon, Thomas
* Moderation: Tom, Ingo
* Protokoll: Tom, Ingo

[TOC]

## Anliegen der Gäste
* Keine Anliegen von Gästen

## Berichte der AKs und TFs

### AK Struktur (Philipp)
* Es gibt nichts neues, AK-Leitung abgemeldet

### AK Demo (Leon A.)
* Es gibt nichts Neues

### AK Mobi (Lara)
* Am Freitag, den 17.04. um 11:00 Uhr findet eine TK statt

### AK Aktionen (Janika)
* Eine TK fand statt und es wurde über verschiedene Möglichkeiten für Aktionen gesprochen
* Für den 24.04. und vor / nach dem 24.04.
* Verbindung zu den Mottotagen des AK Aktionen
* Kreideaktionen / Bannermalen / einige kleinere Sachen
* Fotocollagen 
* Videoproduktion für den 24.04.

### AK Kommunikation (Elias)
* Insta-Motto-Posts sind losgegangen!
* Von der Bundesebene: neue visuelle Identität für FFF

### AK For Future (Alex)
* Es gibt nichts Neues

### TF Strategie (Levin)
* Gestern gab es eine TK, morgen auch wieder: 15.4. 11:00 Uhr, alle sind eingeladen
* Auch strukturelle Themen wurden besprochen, etwa Einteilung in verschiedene Themenbereiche; pro Sektor soll es dann eine Strategie geben
* Leitbildsuche

### Deliberichte
* Alles berichtenswerte hat seinen eigenen TOP

## Abarbeitung der TOPs

### TOP 1: Charitiy Livestream 17.04.

Es gab eine Anfrage dazu, ob wir einen Charity-Livestream restreamen können. Es werden Spenden für Seebrücke, die Tafel,  Johanniter, Das rote Kreuz, Malteser und Ärzte ohne Grenzen gesammelt. Der Livestream wird von den OGs Kitzingen und Volkach organisiert. Der AK-Kommunikation wird auf den sozialen Medien darauf hinweisen.

#### Abstimmungsfrage 1
Es wird mit dem bereits existierenden OG-Google-Konto ein YouTube Kanal für unsere OG erstellt, auf dem wir Videos hochladen können und auch alte hochqualitative Videos zu Archivierungszwecken hochladen können. Wir fühlen uns nicht verpflichtet deshalb regelmäßiger Videoinhalte zu produzieren.

**Ergebnis:** 9 Dafür, 1 Dagegen, 3 Enthaltungen

### TOP 2: Deli Abstimmung (Film-AG Legitimierung)

Lucia // 12.04.2020, 16:34 Uhr

Frist: 23.04.2020

Beschreibung: ℹ️ Nachdem sich die Film-AG am 05.04.2020 in der Deli-TK vorgestellt hat, folgt hier nun die reguläre Abstimmung zur Legitimierung der AG.

▶️ Bitte stimmt bis zum 23.04.2020, 21:00 Uhr ab.

👉 Hier die Kompetenzen der Film-AG: https://fffutu.re/filmag

👉 Hier die Abstimmung: https://forms.gle/kKuvHeM4xXbVePEq8

💡 Die Ansprechpersonen:
• Film-AG: UNSERE Sarah (!!) (https://wa.me/491793212093), Finn (https://fffutu.re/finn), Meo (https://fffutu.re/meo)
• CTF (Prozess): Benny (https://fffutu.re/benny)

Liebe Grüße
Eure CTF 💚

#### Abstimmungsfrage 2 
Stimmen wir der Legitimierung der bundesweiten Film-AG zu?

Ergebnis der Abstimmung: 13 dafür, 0 dagegen, 0 Enthaltungen

### TOP 3: Banneraktion

Beschreibung: Bitte malt Banner für den 24.4.

### TOP 4: Selbstverständnis

Luis // 09.04.2020, 15:56 Uhr

Es gibt keine Frist.

Beschreibung: Ein Selbstverständnis mit unseren Zielen und Werten wäre doch geil. Die Idee ist während der Strategie TK aufgekommen. Ich habe schon mal ein paar Dinge gesammelt. 

#### Abstimmungsfrage 4 
Sollen wir ein Selbstverständnis ausarbeiten?

**Ergebnis:**: 13 Dafür, 0 Dagegen, 0 Enthaltungen

#### Abstimmungsfrage 4.2
Das Selbstverständnis muss per Konsens angenommen werden.

**Ergebnis:**: 11 Dafür, 0 Dagegen, 1 Enthaltungen

### TOP 5: Deli-Rat

Luis (den Delis) // 11.04.2020, 18:38 Uhr

Es gibt keine Frist.

Beschreibung: Wir Delis würden gerne bestimmte Abstimmungen vom Plenum trennen. Es handelt sich zunächst um Strukturabstimmungen aus der Bundesstruktur, da die Bundesstruktur deutlich komplexer und umfangreicher ist als unsere OG interne Struktur und wir denken, dass diese Abstimmungen das Plenum nicht interessiert und inhaltlich überfordern. Deshalb würden wir gerne eine Gruppe öffnen (den DeliRat) und dort abstimmen.  Es wird dort nur über solche Abstimmungen entschieden, für die das Plenum den DeliRat explizit beauftragt hat (nicht zwangsläufig die einzelnen Abstimmungen, sondern Bereiche, wie z.B. Strukturabstimmungen). Der DeliRat ist dazu berechtigt jede Abstimmung an das Plenum weiterzuleiten. Es dürfen selbstverständlich alle in diese Gruppe hinein die wollen, und sich zu den Abstimmungen informieren. Dies dient dazu, die Abstimmungsqualität unserer OG hochzuhalten und Menschen nicht unnötig zu belästigen oder ihre Zeit unnötig in Anspruch zu nehmen.

#### Abstimmungsfrage 5.1 
Es wird die Kategorie "Kompetente Arbeitsgruppe für komplexe Abstimmungen" (kurz Kafka) eingerichtet, in welche alle Plenumsmitglieder bedingungslos und stimmberechtigt hinein können. Eine Kafka entscheidet über ein bestimmtes Thema eigenständig in Vertretung des Plenums. Das Plenum entscheidet pro Themenbereich, ob es an eine Kafka delegiert wird. Wenn eine Kafka etwas entscheidet wird darüber schriftlich Bericht erstattet. Der Bericht wird im Plenum verlesen und in die Plenumsgruppe geschickt. Das Plenum kann jederzeit einzelne Abstimmungen, die eigentlich in einer zum Themenbereich gehörenden Kafka entschieden worden wären zurück ins Plenum holen. Die Kafka wird dadurch nicht aufgelöst, es wird lediglich eine einzelne Abstimmung statt in der Kafka im Plenum durchgeführt. Das Plenum kann Kafkas jederzeit durch eine einfache Mehrheit im Plenum auflösen. Eine Kafka kann sich selbst mit einer einfachen Mehrheit innerhalb der Kafka auflösen.

**Ergebnis:** 13 Dafür, 0 Dagegen, 0 Enthaltung

#### Abstimmungsfrage 5.2
Der Themenbereich "Strukturänderungen an der Bundesebenenstruktur" wird an eine Kafka delegiert. Der Deli Luis kümmert sich darum, dass Diskussionen und Abstimmungen dieser Kafka stattfinden. Er erstellt eine Gruppe und lädt alle Plenumsmitglieder dazu ein.

**Ergebnis:** 13 Dafür, 0 Dagegen, 1 Enthaltung

### Neue Visuelle Identität von FFF
* Noah veranstaltet für uns OG-intern eine TK in der er die neue visuelle Identität von FFF erklärt
* Morgen findet die bundesweite Präsentation statt, von der Noah dann berichten wird

## Sonstiges


===========


Das Plenumsprotokoll wurde verlesen: Nein, aber alle konnten am kollaborativen CryptPad in Echtzeit mitlesen.

Das Plenum endete um 22:05 Uhr

## Nach dem Plenum
1. Platzhalter füllen
2. Rechtschreibung prüfen
3. Formatierung ggf. anpassen ([Formatierungshinweise](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet))
4. Den Abschnitt "Nach dem Plenum" löschen
5. Protokoll als "Read-Only", also "Ansehen" per Link teilen und an AG Webseite des AK Kommunikation schicken. **Nicht den Link aus der Adresszeile kopieren! Die "Teilen"-Funktion von CryptPad verwenden!**
6. Link auch in den Telegram-Plenumschat posten