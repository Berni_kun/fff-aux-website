# 2020-06-23 Protokoll Plenum

* Anwesende AK-Leitungen: Leon, Lara, Alex
* Anwesende Delis: Luis, Sophia
* Abwesend: Philipp
* Moderation: Sophia
* Protokoll: Tom

[TOC]

## Anliegen der Gäste
* Keine Gäste anwesend

## Berichte der AKs und TFs

### AK Struktur (Philipp)
* Derzeit nichts neues

### AK Demo (Leon A.)
* Wird in einem eigenen TOP besprochen

### AK Mobi (Lara)
* Derzeit nichts neues

### AK Aktionen (Janika)

### AK Kommunikation (Elias)
* Derzeit nichts neues

### AK For Future (Alex)
* Die Stadtregierung wollte mit Radentscheidvertretenden über die Forderungen reden, das Gespräch hat stattgefunden
* Die neue OB Weber enttäuscht uns, da sie Fahrradfahrern doch nicht helfen will "um Autofahrer nicht zu benachteiligen"

### TF Strategie (Eluias)
* Es gibt voraussichtlich Freitag ein Treffen zur Strategie

### Deliberichte (Lucia, Janika, Sophia)
* Wird nachgeholt, sobald unsere Delis wieder anwesend sind

### Kafka-Berichte
* Derzeit nichts zu berichten


## Abarbeitung der TOPs

### TOP 1: Vorstellung der möglichen Schlussfolgerungen aus der Umfrage zu unseren Zielen

Die Ergebnisse der TF-Strategie kann man in [diesem Protokoll](https://cryptpad.rebellieren.jetzt/pad/#/2/pad/view/kPjtq2p95lG5kG9866S8ONhQt7GNM+jdJ3Tc6Z7OLec/embed/) nachlesen. 



### TOP 2: Stadtradeln

Es gibt beim Stadtradeln ein Team "#Fahrradstadt jetzt! - Radentscheid Augsburg". Dort machen wir mit und bewerben dies bzw. erstellen ein Unterteam für uns, sofern die Kilometer dann auch für das Oberteam "#Fahrradstadt jetzt! - Radentscheid Augsburg" gezählt werden.



### TOP 3: Fahrrad Demo

> Wir hatten eine super produktive TK in der wir uns entschieden am Sonntag um 14:00Uhr eine Fahrraddemo zu organisieren. 
Protokoll: https://cryptpad.rebellieren.jetzt/pad/#/2/pad/view/dzH13MdNqBkPf1hj0MTfkxR0GG06nebBUfXhDRHaGw8/

#### Abstimmungsfrage 3

Es soll am Sonntag, den 28.06.2020 um 19:00 Uhr eine Fahrraddemo stattfinden. Sie soll von Robil angemeldet werden. Wenn dies nicht möglich ist, soll Ingo die Demo anmelden. Die Demostrecke soll über die B17 gehen.

* **Ergebnis:** 16 dafür, 0 dagegen, 0 enthaltungen



### TOP 4: Anfrage Black Lives Matter Material

BLM Augsburg fragt uns wegen Ordnerbinden, Lauti und Werbung

#### Abstimmungsfrage 4
Wir leihen den Organisator:innen von Black Lives Matter Augsburg für ihre nächste Demo am Samstag unsere Ordnerbinden, unsere Lautsprecheranlage und bewerben ihre Demo über die entsprechenden Kanäle. Dabei betonen wir, dass wir das Equipment für unsere eigene Demo am nächsten Tag (Sonntag, 28.06.2020 um 19:00 Uhr) zurück benötigen. Auch hier gilt, wie bei unseren eigenen Demos, dass bei allen Hinweisen auf Veranstaltungen auf das Tragen eines Mund-Nasen-Schutzes sowie die Einhaltung des Mindestabstands hinweisen.

* **Ergebnis**: Dafür 8, Dagegen 0, Enthaltung 0

## Sonstiges
* Philipp übergibt das derzeit inaktive Twitterkonto an Leon A. und Tom, welche zumindest Retweets von FFF Detuschland schaffen werden.

Das Protokoll wurde verlesen: Nein, aber alle konnten am kollaborativen CryptPad in Echtzeit mitlesen.

Das Plenum endete um 23:30 Uhr