#!/usr/bin/env perl

use warnings;
use strict;

my $i = 0;

sub put {
    my ($color, $col2) = @_;

    print "<div class=\"pixel\" style=\"background-color: $color; border-color: $col2\"></div>";
    $i++;
    print "<br>\n" if $i % 100 == 0;
}

print <<EOF;
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<style>
.pixel { width: 5px; height: 5px; border: 0.5px solid black; display: inline-block; margin: 0; padding: 0; }
td { vertical-align: top; }
th { text-align: left; }
body { font-family: sans-serif; }
</style>
</head>
<body>

<table>
<tr>
<td>
EOF

put("yellow", "#aa0") for 1..0.78084 * 10000;
put("blue", "darkblue") for 1..0.20942 * 10000;
put("#5c4673", "#262334") for 1..0.00934 * 10000;
put("red", "darkred") for 1..0.00040 * 10000;
put("gray", "black");

print <<EOF;
</td>
<td>
<strong>Zusammensetzung trockener Luft</strong><br>
<br>
<table>
  <tr style="background-color: yellow"><th>Stickstoff</th><td>N₂</td><td>78,1
  %</td><td>781.000 ppm</td></tr>
  <tr style="background-color: blue; color:
  white"><th>Sauerstoff</th><td>O₂</td><td> 20,9 %</td><td>209.000 ppm</td></tr>
  <tr style="background-color: #5c4673; color:
  white"><th>Argon</th><td>Ar</td><td>&#8199;0,93 %</td><td>&#8199;&#8199;9.300 ppm</td></tr>
  <tr style="background-color:
  red"><th>Kohlendioxid</th><td>CO₂</td><td>&#8199;0,04
  %</td><td>&#8199;&#8199;&#8199;&#x2008;400 ppm</td></tr>
</td>
</tr>
</table>
</body>
</html>
EOF
